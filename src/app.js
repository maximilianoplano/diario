"use strict";
require("dotenv").config();

const express = require("express");
const cookieParser = require("cookie-parser");
const Task = require("./models/task")

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.get("/tasks", async (req, res, next) => {
    Task.findAll({ where: { is_finished: false } }).then(tasks => {
        res.send(tasks)
    })
});

app.post("/tasks", (req, res, next) => {
    Task.create({
        description: req.body.description,
        is_finished: false
    }).then(task => {
        res.send(task);
    })
});

app.put("/tasks/:task_id/finished", (req, res, next) => {
    Task.update({ is_finished: true }, { where: { id: req.params.task_id } })
        .then(result => {
            res.send(result)
        });
});


app.get("/health", async (req, res, next) => {
    res.send({ healthy: true });
});

module.exports = app;
