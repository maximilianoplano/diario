const Sequelize = require("sequelize");
const config = require("./config");

const dbConnection = new Sequelize(config.DATABASE_URL, {
  logging: false
});

module.exports = dbConnection;
