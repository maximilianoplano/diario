const Task = require('../src/models/task');

const createTask = (description, is_finished) => {
  return Task.create({ description, is_finished });
}

module.exports = { createTask }
