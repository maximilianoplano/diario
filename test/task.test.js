const request = require("supertest");
const assert = require("chai").assert;
const app = require("../src/app");
const { createTask } = require("./utils");
const Task = require("../src/models/task")

describe("Tasks", () => {
    it("should create a new task", async () => {
        const response = await request(app)
            .post("/tasks")
            .send({
                description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
            })
            .expect(200);

        const task = await Task.findOne({ where: { id: response.body.id } });

        assert.isNotNull(task);
        assert.deepStrictEqual(task.description, "Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
        assert.deepStrictEqual(task.is_finished, false);
        await task.destroy();
    });

    it("should update a task", async () => {
        const task = await createTask("description", false);

        await request(app)
            .put(`/tasks/${task.id}/finished`)
            .expect(200);

        await task.reload();
        assert.deepStrictEqual(task.description, "description");
        assert.deepStrictEqual(task.is_finished, true);
        await task.destroy();
    });

    it("should return unfinished tasks", async () => {
        const falseTask = await createTask("description", false);
        const trueTask = await createTask("description", true);

        const response = await request(app)
            .get("/tasks")
            .expect(200);

        assert.deepStrictEqual(response.body.length, 1)
        assert.deepStrictEqual(response.body[0].description, "description");
        assert.deepStrictEqual(response.body[0].is_finished, false);
        await trueTask.destroy();
        await falseTask.destroy();
    });
});
