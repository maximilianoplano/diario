const assert = require('assert');
const request = require('supertest');
const app = require("../src/app");

describe('Health Check', function () {
  it('should return healthy true', function () {
    request(app)
      .get('/health')
      .expect(200)
      .then(response => {
        assert(response.body.healthy, true)
      })
  });
});
