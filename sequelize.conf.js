const config = require("./src/config/config");


module.exports = {
  development: {
    url: config.DATABASE_URL
  },
  test: {
    url: config.DATABASE_URL
  },
  production: {
    url: config.DATABASE_URL
  }
};
